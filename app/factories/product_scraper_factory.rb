#factory class for scraping product. Right now only works for walmart products. No additional logic required
class ProductScraperFactory

  def self.get_scraper name,product_url
    WalmartProductScraper.new(product_url)
  end

end