class ScraperProduct < GenericWrapper#generic product which will create product with given externat_id and import products reviews for tegret site

  def initialize(product, scraper)
    super(product)
    @scraper = ReviewScraperFactory.get_scraper(scraper,@model.w_id)
  end

  def save
    @model.save && create_reviews
  end

  private

  def import_reviews
    @reviews = @scraper.import_reviews
  end

  def create_reviews
    import_reviews
    @model.reviews.create(@reviews)
  end
end
