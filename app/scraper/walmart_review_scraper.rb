#Module nokogiri utils parses data from walmart page
class WalmartReviewScraper
   WALMART_REVIEWS_URL ="https://www.walmart.com/reviews/product/" #url of product review page
   LAST_PAGE = "a.js-pagination" #go to the pagination
   CUSTOMER_REVIEW = ".grid.customer-review.js-customer-review"#tag for review
   CUSTOMER_REVIEW_TITLE = ".customer-review-title" #tag for title review
   CUSTOMER_REVIEW_DATE = ".customer-review-date" #tag for date review
   CUSTOMER_REVIEW_TEXT = ".customer-review-text" #tag for text review
   PAGING_PARAM =  "?page=" #pagination param
   INIT = 1 #starting paging page


  def initialize w_id
    @w_id = w_id
    @reviews = Array.new
  end

  def import_reviews #return all reviews
    doc = get_doc INIT
    collect_reviews doc
    last_page = doc.css(LAST_PAGE).last

    return @reviews if last_page.blank? || last_page.text.to_i == INIT

    for i in (INIT+1)..last_page.text.to_i do
      import_page_reviews i
    end

    @reviews

  end

  private

  def import_page_reviews page_no #retrieve reviews for products
    doc = get_doc page_no
    collect_reviews doc

  end

  def get_doc page_no #open page content via nokogiri
    page = WALMART_REVIEWS_URL + @w_id.to_s + PAGING_PARAM + page_no.to_s
    Nokogiri::HTML(open(page))
  end

  def collect_reviews doc #parse oage content to match review box
    doc.css(CUSTOMER_REVIEW).each do |review|
      title =  review.css(CUSTOMER_REVIEW_TITLE).text
      date =  review.css(CUSTOMER_REVIEW_DATE).text
      date = Date.strptime(date, '%m/%d/%Y')
      text =  review.css(CUSTOMER_REVIEW_TEXT).text
      @reviews.push({title: title, creation_date: date, text:text })
    end
  end

end
