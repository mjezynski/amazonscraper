#retrieves product title and urls. Name of the product could be as well retrieved from url
class WalmartProductScraper
  TITLE = "h1.product-name.product-heading.js-product-heading" #tag where title

  attr_reader :product_title, :product_id

  def initialize url
     @url = url
     get_product_title
     get_product_id
  end

  private

  def get_product_title
    doc = Nokogiri::HTML(open(@url))
    @product_title = doc.css(TITLE).text
  end

  def get_product_id
    @product_id = @url.split("/").last.split("?").first
  end

end