require 'nokogiri'
require 'open-uri'
#Handles requests for product
class ProductsController < ApplicationController
  respond_to :html,:js

  def create
    prod = ScraperProduct.new(Product.new(w_id:params[:w_id], title:params[:title]), ReviewScraperFactory::WALMART_SCRAPER)
    prod.save
    redirect_to prod
  end

  def index
    @products = Product.all
  end

  #display title of product
  def get_title
    @scraper = ProductScraperFactory.get_scraper(ReviewScraperFactory::WALMART_SCRAPER, params[:url])
  end

  def show
    @product = Product.find params[:id]
  end
end