class CreateReview < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.text :title, null:false
      t.text :text, null:false
      t.integer :product_id, null:false
      t.date :creation_date, null:false
      t.timestamps
    end
  end
end
