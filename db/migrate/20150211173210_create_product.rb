class CreateProduct < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.text :title, null:false
      t.integer :w_id, :limit => 8, null:false
      t.timestamps
    end
  end
end
